#include <iostream>
#include <string>
using namespace std;

string translate(string line, string argument){ 
    unsigned int toAdd = std::stoi(argument);
    
    for(unsigned int i=0; i < line.size(); i++){
        for(unsigned int j=0; j < toAdd; j++){
            if(line[i] == 46){
                continue;
            }
            else{
                line[i] += 1;
                if(line[i] == 91){
                    line[i] = 65;
                }
                else if(line[i] == 123){
                    line[i] = 97;
                }
                else{
                    continue;
                }
            }
        }
    }
    string result = line;
    return result; }


int main(int argc, char *argv[]){ 
    string line;
    if(argc != 2){ 
        cerr << "Deze functie heeft exact 1 argument nodig" << endl;
        return -1; 
    }

    while(getline(cin, line)){ 
        cout << translate(line, argv[1]) << endl; 
    } 

    return 0; 
}