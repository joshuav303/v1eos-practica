#include "shell.hh"

int main(){ 
    std::string input;
  
    while(true){
        int fd = syscall(SYS_open, "welkom.txt", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.

        char prompt[1];
        while(syscall(SYS_read, fd, prompt, 1)){
          std::cout << prompt;                  // Print het prompt
        }

        std::getline(std::cin, input);         // Lees een regel
        if (input == "new_file") new_file();   // Kies de functie
        else if (input == "ls") list();        //   op basis van
        else if (input == "src") src();        //   de invoer
        else if (input == "find") find();
        else if (input == "seek") seek();
        else if (input == "exit") return 0;
        else if (input == "quit") return 0;
        else if (input == "error") return 1;

        if (std::cin.eof()) return 0; } }      // EOF is een exit

void new_file(){
    std::string bestandsnaam = "";
    std::string text = ""; 
    std::cout << "Geef een bestandsnaam: ";
    std::getline(std::cin, bestandsnaam);    
        
    int fd = syscall(SYS_open, bestandsnaam.c_str(), O_RDWR | O_CREAT, S_IRWXU);
    
    while(true){
        std::cout << "Voer de tekst in: ";
        std::getline(std::cin, text);
        if(text.find("<EOF>") != std::string::npos){
            syscall(SYS_write, fd, text.c_str(), text.size()-5);
            break;
        }
        else{
            syscall(SYS_write, fd, text.c_str(), text.size());
        }
        text = "\n";
        syscall(SYS_write, fd, text.c_str(), text.size());
    }
}


void list(){
    if (syscall(SYS_fork) == 0){
        int pid = syscall(SYS_fork);
        if (pid < 0){
            syscall(SYS_exit,0);
        }
        if (pid == 0){
            const char *args[] = {"/bin/ls","-la", (char *) 0 };
            syscall(SYS_execve,"/bin/ls", args, 0);
            syscall(SYS_exit,0);
        }
        else{
            syscall(SYS_wait4, pid,0,0,0);
        }
    }
}


void find(){   
    int fd[2];
    syscall(SYS_pipe, fd);
    
    std::string zoek = "";
    std::cout << "Voer een string in: ";
    std::getline(std::cin, zoek);  
    
    int pid1 = syscall(SYS_fork);
    
    if (pid1 == 0){
        const char *args1[] = {"/usr/bin/find",".", (char *) 0 };
        syscall(SYS_dup2, fd[1], 1);
        syscall(SYS_close, fd[0]);
        syscall(SYS_execve,"/usr/bin/find", args1, 0);
    }
    else{
        syscall(SYS_wait4, pid1,0,0,0);
    }
        
    int pid2 = syscall(SYS_fork);
    
    if (pid2 == 0){
        const char *args2[] = {"/bin/grep",zoek.c_str(), (char *) 0 };
        syscall(SYS_dup2, fd[0], 0);
        syscall(SYS_close, fd[1]);
        syscall(SYS_execve,"/bin/grep", args2, 0);
    }
    else{
        syscall(SYS_close, fd[1]);
        syscall(SYS_close, fd[0]);
        syscall(SYS_wait4, pid2,0,0,0);
    }
}


void seek(){ 
    int fd2 = syscall(SYS_open, "loop", O_RDWR | O_CREAT, S_IRWXU);
    syscall(SYS_write, fd2, "x", 1);
    for(unsigned int i=0; i < 5000000; i++){
        syscall(SYS_write, fd2, "\0", 1);
    }
    syscall(SYS_write, fd2, "x", 1);
    //std::cout << "Loop klaar" << std::endl;
    
    int fd1 = syscall(SYS_open, "seek", O_RDWR | O_CREAT, S_IRWXU);
    
    syscall(SYS_write, fd1, "x", 1);
    syscall(SYS_lseek, fd1, 5000000, 1);                             // Seek is een stuk sneller.
    syscall(SYS_write, fd1, "x", 1);                                        // ls -lh geeft aan dat de bestanden dezelfde grote hebben namelijk 5.1M
    //std::cout << "Syscall klaar" << std::endl;                                           // ls -lS geeft aan dat seek.txt 1 byte kleiner is.
}


void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1))                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    std::cout << byte; }                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.